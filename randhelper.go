//++++++++++++++++++++++++++++++++++++++++
//Fighting for great,share generate value!
//Build the best soft by golang,let's go!
//++++++++++++++++++++++++++++++++++++++++
//Author:ShirDon <http://www.shirdon.com>
//Email:hcbsts@163.com;  823923263@qq.com
//++++++++++++++++++++++++++++++++++++++++

package captchas_with_go

import (
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// rnd returns a non-crypto pseudorandom int in range [from, to].
func rnd(from, to int) int {
	return rand.Intn(to+1-from) + from
}

// rndf returns a non-crypto pseudorandom float64 in range [from, to].
func rndf(from, to float64) float64 {
	return (to-from)*rand.Float64() + from
}

func randStr(length int) string {
	rst := ""
	charset := "abcdefghijklmnopqrstuvwxyz1234567890"
	for i := 0; i < length; i++ {
		rst = rst + string(charset[rnd(0, len(charset)-1)])
	}
	return rst
}
